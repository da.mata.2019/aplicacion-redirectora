#!/usr/bin/python

import socket
import random

PORT = 1234

urls = ['https://www.google.com','https://www.instagram.com',
    'https://www.microsoft.com','https://www.realmadrid.com'
]


def url_random():
    url = random.choice(urls)
    send = "HTTP/1.1 307 Temporary Redirect \r\n" \
           + "Location: " + url + "\r\n"
    return send

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.bind(('localhost', PORT))

mySocket.listen(5)


try:
    while True:
        print("Waiting for connections")
        (recvSocket, address) = mySocket.accept()
        print("HTTP request received:")
        print(recvSocket.recv(2048))
        recvSocket.send(url_random().encode('ascii'))
        recvSocket.close()

except KeyboardInterrupt:
	print("Closing binded socket")
	mySocket.close()
